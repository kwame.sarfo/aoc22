package Day5;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.*;

public class Day5 {
    public static void main(String[] args) throws IOException {
       partOne();

       partTwo();
    }


    public static void partOne() throws IOException {
        List<String> lines = Files.readAllLines(Path.of("src/main/resources/stack-input.txt"));


        Map<Integer, Stack<String>> stack = new HashMap<>();


        int iter = 0;
        int hashIndex = 1;


        //8
        for(int i = 7; i>=0; i--) {
            String line = lines.get(i);

            String[] splittedLine = line.split(" ");

            for (int j = 0; j< splittedLine.length; j++)
            {
                if(!stack.containsKey(j+1))
                {
                    stack.put(j+1, new Stack());
                    if(!splittedLine[j].equalsIgnoreCase("-"))
                    {
                        stack.get(j+1).push(splittedLine[j]);
                    }
                }
                else {
                    if(!splittedLine[j].equalsIgnoreCase("-"))
                    {
                        stack.get(j+1).push(splittedLine[j]);
                    }
                }
            }
        }

        //10
        for(int i = 10; i<lines.size(); i++)
        {
            String line = lines.get(i);

            String[] splittedLine = line.split(" ");

            int moveQuantity = Integer.valueOf(splittedLine[1]);

            int origin = Integer.valueOf(splittedLine[3]);

            int destination = Integer.valueOf(splittedLine[5]);


            int moveIterator = 0;


            while(moveIterator < moveQuantity)
            {
                String toBeRemoved = stack.get(origin).pop();
                stack.get(destination).push(toBeRemoved);
                moveIterator+=1;
            }
        }

        String solution = "";

        for(Stack col : stack.values())
        {
            solution+=col.pop();
        }

        System.out.println(solution);
    }


    public static void partTwo() throws IOException {
        List<String> lines = Files.readAllLines(Path.of("src/main/resources/stack-input.txt"));


        Map<Integer, Stack<String>> stack = new HashMap<>();


        for(int i = 7; i>=0; i--) {
            String line = lines.get(i);

            String[] splittedLine = line.split(" ");

            for (int j = 0; j< splittedLine.length; j++)
            {
                if(!stack.containsKey(j+1))
                {
                    stack.put(j+1, new Stack());
                    if(!splittedLine[j].equalsIgnoreCase("-"))
                    {
                        stack.get(j+1).add(splittedLine[j]);
                    }
                }
                else {
                    if(!splittedLine[j].equalsIgnoreCase("-"))
                    {
                        stack.get(j+1).add(splittedLine[j]);
                    }
                }
            }
        }


        for(int i = 10; i<lines.size(); i++)
        {
            String line = lines.get(i);

            String[] splittedLine = line.split(" ");

            int moveQuantity = Integer.valueOf(splittedLine[1]);

            int origin = Integer.valueOf(splittedLine[3]);

            int destination = Integer.valueOf(splittedLine[5]);


            int moveIterator = 0;

            List<String> removeList = new ArrayList<>();
            while(moveIterator < moveQuantity)
            {
                removeList.add(stack.get(origin).pop());
                moveIterator+=1;
            }


            for(int x = removeList.size()-1; x>=0; x--)
            {
                stack.get(destination).push(removeList.get(x));
            }
        }


        String solution = "";

        for(Stack col : stack.values())
        {
            solution+=col.pop();
        }

        System.out.println(solution);
    }
}
